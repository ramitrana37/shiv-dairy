import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shivdairystoreapp/screens/splashscreen.dart';

import 'providers/cart.dart';
import 'providers/products.dart';
import 'screens/CartScreen.dart';
import 'screens/OnBoardingScreens.dart';


//import 'package:shivdairystoreapp/providers/Products.dart';
//import 'screens/Products_overview_Screen.dart';
import 'screens/Products_overview_Screen.dart';
import 'screens/productsDetailsScreen.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
        providers:[
          ChangeNotifierProvider.value(value:Products(),),
          ChangeNotifierProvider.value(value:Cart()),
        ],
      child: MaterialApp(
debugShowCheckedModeBanner: false,
        title:'Shiv Dairy',
        initialRoute:ProductsOverViewScreen.screenId,
//      OnBoardingScreens.id,
//      initialRoute:ProductsOverviewScreen.screenId,
        routes: {
          SplashScreen.screenId:(context)=>SplashScreen(),
          OnBoardingScreens.screenId:(context)=>OnBoardingScreens(),
          ProductsOverViewScreen.screenId:(context)=>ProductsOverViewScreen(),
          ProductsDetailsScreen.screenId:(context)=>ProductsDetailsScreen(),
          CartScreen.screenId:(context)=>CartScreen(),
        },
      ),
    );
  }
}
