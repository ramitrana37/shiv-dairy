//import 'package:flutter/material.dart';
//import 'package:provider/provider.dart';
//import 'package:shivdairystoreapp/providers/Products.dart';
//
//
//
//
//class ProductDetailsScreens extends StatelessWidget {
//  static const screenId='ProductDetailsScreens';
//
//
//  @override
//  Widget build(BuildContext context) {
//    final productId=ModalRoute.of(context).settings.arguments as String;
//    final loadedProduct=Provider.of<Products>(context).allItems.firstWhere((prod) => prod.id==productId);
//    return Scaffold(
//      appBar: AppBar(
//        title: Text(loadedProduct.title),
//        backgroundColor: Colors.transparent,
//      ),
//      body: Container(
//        child: Column(children: [
//          Card(
//            child:Image.network(loadedProduct.imageUrl),
//          ),
//          Text(loadedProduct.title),
//        ],
//        ),
//      ),
//    );
//  }
//}


import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shivdairystoreapp/providers/products.dart';
class ProductsDetailsScreen extends StatelessWidget {
  static const screenId='ProductDetailsScreens';
  @override
  Widget build(BuildContext context) {
    final String productId=ModalRoute.of(context).settings.arguments;
final loadedProduct=Provider.of<Products>(context,listen: false).findById(productId);
    return Scaffold(
      appBar: AppBar(title: Text(loadedProduct.title),),
      body: Column(
        children:<Widget> [
            Padding(
              padding: const EdgeInsets.only(left:20,right: 20,top: 50 ),
              child: Row(
                children: [
                IconButton(icon: Icon(
                  Icons.menu,
                  color: Colors.black,
                  size: 30 ,

                ),
                  onPressed: () { },
                ),
                Spacer(),
                  Icon(
                    Icons.shopping_cart
                  )
              ],
              ),
            ),
          SizedBox(height:20 ,),
          Padding(
            padding: const EdgeInsets.only(left:8.0,right: 20),
            child: Container(
              width: MediaQuery.of(context).size.width, 
              height: 50.0,
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(30),
                 border: Border.all(color: Colors.grey,width: 0.5),
              ),
              child: Row(
                children: <Widget>[
                  Text('Search here',style: TextStyle(
                    color: Colors.grey,
                    fontSize: 10,
                  ),),
                  Spacer(),
                  Icon(Icons.search,color: Colors.black ,)

                ],
              ),
            ),
          ),
          Container(child: Text(loadedProduct.title),)
      ],
      ),
    );
  }
}
