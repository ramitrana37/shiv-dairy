import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';
//import 'package:shivdairystoreapp/main.dart';
//import 'package:shivdairystoreapp/screens/homepage.dart';


class SplashScreen extends StatefulWidget {
  static const String screenId='SplashScreen';
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  // check for user is already register or not
  void _mockCheckForSession() async {
    await Future.delayed(Duration(milliseconds:5000),(){}
    );
  }




@override
  void initState() {
_mockCheckForSession();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        child:Column(
          children: [
            Stack(
              alignment: Alignment.topCenter,
              children: <Widget>[
                Opacity(
                  opacity: 0.5,
                  child: Image.asset('images/bg.jpeg'),
                ),
              ],
            ),
            Shimmer.fromColors(
              baseColor: Colors.green.shade900,
              highlightColor:Colors.green.shade700,
              child:Container(
                child: Text('Shiv Dairy Store',
                  style: TextStyle(
                    fontSize: 45,
                    fontFamily: 'Pacifico',
                    shadows: <Shadow>[
                      Shadow(
                          blurRadius: 20,
                        color:Colors.black87,
                        offset: Offset.fromDirection(100,0),
                      ),
                    ]
                  ),
                ),
              ),

            ),
            Expanded(child: SizedBox(height: 50,)),
            SafeArea(child: Text('Made in India ',style: TextStyle(fontSize:8 ),)),
          ],
        ) ,
      ),
    );
  }
}
