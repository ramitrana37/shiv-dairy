import 'package:flutter/material.dart';
import 'package:introduction_screen/introduction_screen.dart';

class OnBoardingScreens extends StatelessWidget {
  static const String screenId = '/OnBoardingScreens';
  @override
  Widget build(BuildContext context) {
    return SliderTile();
  }
}

class SliderTile extends StatelessWidget {
final pageDecoration=PageDecoration(

  imageFlex: 2,


);
List <PageViewModel> getPages(){
  return[
    PageViewModel(
      image: Image.asset('images/shoppingapp.png'),
      title: 'Buying Now Easier than before',
      body: 'Buy Your Monthly Grocery In Single Click. \n No More Need To Add Grocery Every Time Every Month.Now We Have 2000 Products From Different Brands and Names ',
      footer: Text('Shiv Dairy Store User Reserved program',style: TextStyle(fontSize:12,fontWeight: FontWeight.bold,color: Colors.black54 ),),
      decoration: pageDecoration,

    ),
    PageViewModel(
      image: Image.asset('images/amazing offers.jpg'),
      title: 'Attractive Offer And Deals ',
      body: 'Buy Your Monthly Grocery In Single Click. \n No More Need To Add Grocery Every Time Every Month.Now We Have 2000 Products From Different Brands and Names ',
      footer: Text('Shiv Dairy Store User Reserved program',style: TextStyle(fontSize:12,fontWeight: FontWeight.bold,color: Colors.black54 ),),
      decoration: pageDecoration,

    ),
    PageViewModel(
      image: Image.asset('images/wallet.png'),
      title: 'Pay By Various Wallets like Paytm',
      body: 'Buy Your Monthly Grocery In Single Click. \n No More Need To Add Grocery Every Time Every Month.Now We Have 2000 Products From Different Brands and Names ',
      footer: Text('Shiv Dairy Store User Reserved program',style: TextStyle(fontSize:12,fontWeight: FontWeight.bold,color: Colors.black54 ),),
      decoration: pageDecoration,

    ),
    PageViewModel(
      image: Image.asset('images/ordercolor.jpg'),
      title: 'Buying Now Easier than before',
      body: 'Buy Your Monthly Grocery In Single Click. \n No More Need To Add Grocery Every Time Every Month.Now We Have 2000 Products From Different Brands and Names ',
      footer: Text('Shiv Dairy Store User Reserved program',style: TextStyle(fontSize:12,fontWeight: FontWeight.bold,color: Colors.black54 ),),
      decoration: pageDecoration,

    ),
    PageViewModel(
      image: Image.asset('images/mynotifications.png'),
      title: 'Buying Now Easier than before',
      body: 'Buy Your Monthly Grocery In Single Click. \n No More Need To Add Grocery Every Time Every Month.Now We Have 2000 Products From Different Brands and Names ',
      footer: Text('Shiv Dairy Store User Reserved program',style: TextStyle(fontSize:12,fontWeight: FontWeight.bold,color: Colors.black54 ),),
      decoration: pageDecoration,

    ),

  ];
}
  @override
  Widget build(BuildContext context) {
    return IntroductionScreen(
//      globalBackgroundColor: Color(0xFF90C0DC),
        pages: getPages(),
        onDone: (){},
        done: Text('Next',style: TextStyle(fontSize: 14,color: Colors.greenAccent,),)
    );
  }
}
