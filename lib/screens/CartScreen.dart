import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shivdairystoreapp/providers/cart.dart';

import 'cartcards.dart';



class CartScreen extends StatelessWidget {
  static const screenId='CartScreen';

  @override
  Widget build(BuildContext context) {
    final cartContainer=Provider.of<Cart>(context);

    return Scaffold(
      appBar: AppBar(
        title: Text('Your Cart',style:TextStyle(fontSize: 14)),

      ),
      body:Column(children: [
        SizedBox(height: 10,),
        Expanded(child: ListView.builder(
            itemBuilder: (ctx,index)=>CartCards(
              productId:cartContainer.cartItems.keys.toList()[index],
              id: cartContainer.cartItems.values.toList()[index].id,
              price: cartContainer.cartItems.values.toList()[index].price,
              title: cartContainer.cartItems.values.toList()[index].title,
              quantity: cartContainer.cartItems.values.toList()[index].quantity,
            ),
          itemCount: cartContainer.itemsCount,
        )),
        Card(
          margin: EdgeInsets.all(15),
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(' Total ',style: TextStyle(fontSize: 18),),
                  Spacer(),
                  Chip(label: Text(cartContainer.totalPrice.toStringAsFixed(2)),),
                  FlatButton(child:Text('Order',style:TextStyle(color: Colors.red)),onPressed: null),
                ]
            ),
          ),
        ),
      ],) ,
    );
  }
}

