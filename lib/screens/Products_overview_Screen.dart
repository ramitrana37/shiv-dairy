
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shivdairystoreapp/providers/cart.dart';
import 'package:shivdairystoreapp/widgets/badge.dart';
import 'package:shivdairystoreapp/widgets/product_Grid.dart';

import 'CartScreen.dart';

enum FilterOptions{
favorite,
AllProducts,
}



class ProductsOverViewScreen extends StatefulWidget {
  static const screenId='ProductsOverViewScreen';

  @override
  _ProductsOverViewScreenState createState() => _ProductsOverViewScreenState();
}

class _ProductsOverViewScreenState extends State<ProductsOverViewScreen> {
  bool showFavs=false;
  @override
  Widget build(BuildContext context) {

    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        title: Text('My Shop'),
        actions: [
          PopupMenuButton(

              icon: Icon(Icons.more_vert),
              onSelected: (FilterOptions value){
              setState(() {
                if(value==FilterOptions.favorite)
                {
                  showFavs=true ;
                }
                else
                {showFavs=false;
                }
              });
              },
              itemBuilder: (_)=>[
                PopupMenuItem(child: Text('Favorites'),value:FilterOptions.favorite ,),
                PopupMenuItem(child: Text('All Products'),value:FilterOptions.AllProducts ,)
              ]
          ),
          Consumer<Cart>(
            builder: (_ ,  cart, ch) => Badge(
                child: ch,
                value: cart.itemsCount.toString(),
            ),

            child:IconButton(icon: Icon(Icons.shopping_cart), onPressed: (){Navigator.pushNamed(context,CartScreen.screenId);}),
          ),
        ],
      ),
      body: ProductGrid(showFavs:showFavs),
    );
  }
}


































//import 'package:flutter/material.dart';
//import 'package:provider/provider.dart';
//import 'package:shivdairystoreapp/providers/products.dart';
//
//import 'package:shivdairystoreapp/widgets/product_Grid.dart';
//
//enum FilterOptions{
//  favorite,
//  AllProducts,
//}
//
//
//
//
//class ProductsOverViewScreen extends StatelessWidget {
//static const screenId='ProductsOverViewScreen';
//  @override
//  Widget build(BuildContext context) {
//    final productContainer=Provider.of<Products>(context,listen: false);
//    return Scaffold(
//      backgroundColor: Colors.black,
//      appBar: AppBar(
//        title: Text('My Shop'),
//        actions: [
//          PopupMenuButton(
//
//            icon: Icon(Icons.more_vert),
//              onSelected: (FilterOptions value){
//              if(value==FilterOptions.favorite)
//                {
//                  productContainer.showFavoriteOnly();
//                }else{productContainer.allProducts();}
//              },
//              itemBuilder: (_)=>[
//            PopupMenuItem(child: Text('Favorites'),value:FilterOptions.favorite ,),
//            PopupMenuItem(child: Text('All Products'),value:FilterOptions.AllProducts ,)
//          ])
//        ],
//      ),
//      body: ProductGrid(),
//    );
//  }
//}
//
//
//
//
//
