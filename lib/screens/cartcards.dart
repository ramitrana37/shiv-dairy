import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shivdairystoreapp/providers/cart.dart';


class CartCards extends StatelessWidget {
  final String id;
  final String productId;
  final String title;
  final double price;
  final int quantity;

  const CartCards({this.productId, this.id, this.title, this.price, this.quantity}) ;

  @override
  Widget build(BuildContext context) {
    return Dismissible(
      key: ValueKey(id),
      background: Container(
        color:Colors.blue,
        alignment: Alignment.centerRight,
        padding: EdgeInsets.only(right: 20),
        margin: EdgeInsets.symmetric(horizontal: 10,vertical: 4),
        child:Icon(
          Icons.delete,
          size: 40,color: Colors.red,
        ) ,),
      direction: DismissDirection.endToStart,
      onDismissed: (direction){
        Provider.of<Cart>(context,listen: false).removeItem(productId);
      },
      child: Card(
        margin: EdgeInsets.symmetric(horizontal: 10,vertical: 4),
        child: Padding(
          padding: EdgeInsets.all(8),
          child:ListTile(
            leading: CircleAvatar(child:Padding(
              padding: const EdgeInsets.all(8.0),
              child: FittedBox(child: Text(price.toStringAsFixed(2))),
            ),),
            title:Text(title) ,
            subtitle: Text('Total =${(price*quantity)}'),
            trailing: Text(quantity.toString()),

          ) ,
        ),
      ),
    );
  }
}
