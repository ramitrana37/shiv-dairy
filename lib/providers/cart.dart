import 'package:flutter/material.dart';


class CartItem{
  final String id;
  final String title;
  final int quantity;
  final double price;

  CartItem({
    @required this.id,
    @required this.title,
    @required this.quantity,
    @required this.price,


  }
    );

}
class Cart with ChangeNotifier{

  Map<String,CartItem> _cartItems ={};
  Map<String,CartItem> get cartItems=>{..._cartItems};

     void addItemsToCart(String productId,String title,double price){
               if(_cartItems.containsKey(productId)){
                  _cartItems.update(productId, (existingCartItem) => CartItem(

                      id:existingCartItem.id,
                      title: existingCartItem.title,
                      quantity: existingCartItem.quantity+1,
                      price: existingCartItem.price )
                  );
                 }
               else{
                 _cartItems.putIfAbsent(productId, () => CartItem(

                     id:DateTime.now().toString(),
                     title: title,
                     quantity: 1,
                     price: price)
                 );
                 notifyListeners();
               }

      }
      int get itemsCount => _cartItems.length;
double get totalPrice{
  var totalAmount=0.0;
  _cartItems.forEach((key, cartItem) {totalAmount+=cartItem.price*cartItem.quantity;});
  return totalAmount;
}


void removeItem(String productId){
  _cartItems.remove(productId);
  notifyListeners();
}







}