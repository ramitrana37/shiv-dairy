import 'package:flutter/foundation.dart';

import 'Product.dart';



class Products with ChangeNotifier{


  List<Product> _items=[

    Product(
      id: 'p1',
      title: 'Redbyrd Shirt',
      description: 'A red shirt - it is pretty red!',
      price: 29.99,
      imageUrl:
      'https://cdn.pixabay.com/photo/2016/10/02/22/17/red-t-shirt-1710578_1280.jpg', category: null,subCategory: null,
    ),
    Product(
      id: 'p2 ',
      title: 'Redwcr Shirt',
      description: 'A red shirt - it is pretty red!',
      price: 29.99,
      imageUrl:
      'https://cdn.pixabay.com/photo/2016/10/02/22/17/red-t-shirt-1710578_1280.jpg', category: null,subCategory: null,
    ),
    Product(
      id: 'p3',
      title: 'Red wggwShirt',
      description: 'A red shirt - it is pretty red!',
      price: 29.99,
      imageUrl:
      'https://cdn.pixabay.com/photo/2016/10/02/22/17/red-t-shirt-1710578_1280.jpg', category: null,subCategory: null,
    ),
    Product(
      id: 'p4',
      title: 'R Shirt',
      description: 'A red shirt - it is pretty red!',
      price: 29.99,
      imageUrl:
      'https://cdn.pixabay.com/photo/2016/10/02/22/17/red-t-shirt-1710578_1280.jpg', category: null,subCategory: null,
    ),
    Product(
      id: 'p5',
      title: 'Red Shirt',
      description: 'A red shirt - it is pretty red!',
      price: 29.99,
      imageUrl:
      'https://cdn.pixabay.com/photo/2016/10/02/22/17/red-t-shirt-1710578_1280.jpg', category: null,subCategory: null,
    ),
    Product(
      id: 'p6',
      title: 'Trousers',
      description: 'A nice pair of trousers.',
      price: 59.99,
      imageUrl:
      'https://upload.wikimedia.org/wikipedia/commons/thumb/e/e8/Trousers%2C_dress_%28AM_1960.022-8%29.jpg/512px-Trousers%2C_dress_%28AM_1960.022-8%29.jpg',category: null,subCategory: null,
    ),
    Product(
      id: 'p7',
      title: 'Yellow Scarf',
      description: 'Warm and cozy - exactly what you need for the winter.',
      category: null,
      subCategory: null,
      price: 19.99,
      imageUrl:
      'https://live.staticflickr.com/4043/4438260868_cc79b3369d_z.jpg' ,
    ),
    Product(
      id: 'p8',
      title: 'A Pan',
      description: 'Prepare any meal you want.',
      price: 49.99,
      imageUrl:
      'https://upload.wikimedia.org/wikipedia/commons/thumb/1/14/Cast-Iron-Pan.jpg/1024px-Cast-Iron-Pan.jpg',
      category: null,
      subCategory: null,
    ),


    Product(
      id: 'p9',
      title: 'Redbyrd Shirt',
      description: 'A red shirt - it is pretty red!',
      price: 29.99,
      imageUrl:
      'https://cdn.pixabay.com/photo/2016/10/02/22/17/red-t-shirt-1710578_1280.jpg', category: null,subCategory: null,
    ),
    Product(
      id: 'p10',
      title: 'Redwcr Shirt',
      description: 'A red shirt - it is pretty red!',
      price: 29.99,
      imageUrl:
      'https://cdn.pixabay.com/photo/2016/10/02/22/17/red-t-shirt-1710578_1280.jpg', category: null,subCategory: null,
    ),
    Product(
      id: 'p11',
      title: 'Red wggwShirt',
      description: 'A red shirt - it is pretty red!',
      price: 29.99,
      imageUrl:
      'https://cdn.pixabay.com/photo/2016/10/02/22/17/red-t-shirt-1710578_1280.jpg', category: null,subCategory: null,
    ),
    Product(
      id: 'p12',
      title: 'R Shirt',
      description: 'A red shirt - it is pretty red!',
      price: 29.99,
      imageUrl:
      'https://cdn.pixabay.com/photo/2016/10/02/22/17/red-t-shirt-1710578_1280.jpg', category: null,subCategory: null,
    ),
    Product(
      id: 'p13',
      title: 'Red Shirt',
      description: 'A red shirt - it is pretty red!',
      price: 29.99,
      imageUrl:
      'https://cdn.pixabay.com/photo/2016/10/02/22/17/red-t-shirt-1710578_1280.jpg', category: null,subCategory: null,
    ),
    Product(
      id: 'p14',
      title: 'Trousers',
      description: 'A nice pair of trousers.',
      price: 59.99,
      imageUrl:
      'https://upload.wikimedia.org/wikipedia/commons/thumb/e/e8/Trousers%2C_dress_%28AM_1960.022-8%29.jpg/512px-Trousers%2C_dress_%28AM_1960.022-8%29.jpg',category: null,subCategory: null,
    ),
    Product(
      id: 'p15',
      title: 'Yellow Scarf',
      description: 'Warm and cozy - exactly what you need for the winter.',
      category: null,
      subCategory: null,
      price: 19.99,
      imageUrl:
      'https://live.staticflickr.com/4043/4438260868_cc79b3369d_z.jpg' ,
    ),
    Product(
      id: 'p16',
      title: 'A Pan',
      description: 'Prepare any meal you want.',
      price: 49.99,
      imageUrl:
      'https://upload.wikimedia.org/wikipedia/commons/thumb/1/14/Cast-Iron-Pan.jpg/1024px-Cast-Iron-Pan.jpg',
      category: null,
      subCategory: null,
    ),




    Product(
      id: 'p617',
      title: 'Redbyrd Shirt',
      description: 'A red shirt - it is pretty red!',
      price: 29.99,
      imageUrl:
      'https://cdn.pixabay.com/photo/2016/10/02/22/17/red-t-shirt-1710578_1280.jpg', category: null,subCategory: null,
    ),
    Product(
      id: 'p718',
      title: 'Redwcr Shirt',
      description: 'A red shirt - it is pretty red!',
      price: 29.99,
      imageUrl:
      'https://cdn.pixabay.com/photo/2016/10/02/22/17/red-t-shirt-1710578_1280.jpg', category: null,subCategory: null,
    ),
    Product(
      id: 'p819',
      title: 'Red wggwShirt',
      description: 'A red shirt - it is pretty red!',
      price: 29.99,
      imageUrl:
      'https://cdn.pixabay.com/photo/2016/10/02/22/17/red-t-shirt-1710578_1280.jpg', category: null,subCategory: null,
    ),
    Product(
      id: 'p20',
      title: 'R Shirt',
      description: 'A red shirt - it is pretty red!',
      price: 29.99,
      imageUrl:
      'https://cdn.pixabay.com/photo/2016/10/02/22/17/red-t-shirt-1710578_1280.jpg', category: null,subCategory: null,
    ),
    Product(
      id: 'p21',
      title: 'Red Shirt',
      description: 'A red shirt - it is pretty red!',
      price: 29.99,
      imageUrl:
      'https://cdn.pixabay.com/photo/2016/10/02/22/17/red-t-shirt-1710578_1280.jpg', category: null,subCategory: null,
    ),
    Product(
      id: 'p21',
      title: 'Trousers',
      description: 'A nice pair of trousers.',
      price: 59.99,
      imageUrl:
      'https://upload.wikimedia.org/wikipedia/commons/thumb/e/e8/Trousers%2C_dress_%28AM_1960.022-8%29.jpg/512px-Trousers%2C_dress_%28AM_1960.022-8%29.jpg',category: null,subCategory: null,
    ),
    Product(
      id: 'p22',
      title: 'Yellow Scarf',
      description: 'Warm and cozy - exactly what you need for the winter.',
      category: null,
      subCategory: null,
      price: 19.99,
      imageUrl:
      'https://live.staticflickr.com/4043/4438260868_cc79b3369d_z.jpg' ,
    ),
    Product(
      id: 'p23',
      title: 'A Pan',
      description: 'Prepare any meal you want.',
      price: 49.99,
      imageUrl:
      'https://upload.wikimedia.org/wikipedia/commons/thumb/1/14/Cast-Iron-Pan.jpg/1024px-Cast-Iron-Pan.jpg',
      category: null,
      subCategory: null,
    ),






    Product(
      id: 'p24',
      title: 'Redbyrd Shirt',
      description: 'A red shirt - it is pretty red!',
      price: 29.99,
      imageUrl:
      'https://cdn.pixabay.com/photo/2016/10/02/22/17/red-t-shirt-1710578_1280.jpg', category: null,subCategory: null,
    ),
    Product(
      id: 'p25',
      title: 'Redwcr Shirt',
      description: 'A red shirt - it is pretty red!',
      price: 29.99,
      imageUrl:
      'https://cdn.pixabay.com/photo/2016/10/02/22/17/red-t-shirt-1710578_1280.jpg', category: null,subCategory: null,
    ),
    Product(
      id: 'p26',
      title: 'Red wggwShirt',
      description: 'A red shirt - it is pretty red!',
      price: 29.99,
      imageUrl:
      'https://cdn.pixabay.com/photo/2016/10/02/22/17/red-t-shirt-1710578_1280.jpg', category: null,subCategory: null,
    ),
    Product(
      id: 'p27',
      title: 'R Shirt',
      description: 'A red shirt - it is pretty red!',
      price: 29.99,
      imageUrl:
      'https://cdn.pixabay.com/photo/2016/10/02/22/17/red-t-shirt-1710578_1280.jpg', category: null,subCategory: null,
    ),
    Product(
      id: 'p28',
      title: 'Red Shirt',
      description: 'A red shirt - it is pretty red!',
      price: 29.99,
      imageUrl:
      'https://cdn.pixabay.com/photo/2016/10/02/22/17/red-t-shirt-1710578_1280.jpg', category: null,subCategory: null,
    ),
    Product(
      id: 'p29',
      title: 'Trousers',
      description: 'A nice pair of trousers.',
      price: 59.99,
      imageUrl:
      'https://upload.wikimedia.org/wikipedia/commons/thumb/e/e8/Trousers%2C_dress_%28AM_1960.022-8%29.jpg/512px-Trousers%2C_dress_%28AM_1960.022-8%29.jpg',category: null,subCategory: null,
    ),
    Product(
      id: 'p30',
      title: 'Yellow Scarf',
      description: 'Warm and cozy - exactly what you need for the winter.',
      category: null,
      subCategory: null,
      price: 19.99,
      imageUrl:
      'https://live.staticflickr.com/4043/4438260868_cc79b3369d_z.jpg' ,
    ),
    Product(
      id: 'p31',
      title: 'A Pan',
      description: 'Prepare any meal you want.',
      price: 49.99,
      imageUrl:
      'https://upload.wikimedia.org/wikipedia/commons/thumb/1/14/Cast-Iron-Pan.jpg/1024px-Cast-Iron-Pan.jpg',
      category: null,
      subCategory: null,
    ),
  ];

//
//  bool _showFovariteItems=false;

  List<Product> get items  {
//    if(_showFovariteItems){
//      return _items.where((element) => element.isfavorite).toList();
//    }
    return [..._items];
  }
  List<Product> get favs{
    return _items.where((element) => element.isfavorite).toList();

  }


Product findById(String id){
    return items.firstWhere((product) => product.id==id);
  }






}