
import 'package:flutter/foundation.dart';

class Product with ChangeNotifier {
 final String id;
 final String title;
 final String description;
 final double price;
 final String imageUrl;
 final String category;
 final String subCategory;
 bool isfavorite;

  Product({@required this.id,@required this.title,@required this.description,@required this.price,@required this.imageUrl,@required this.category,@required this.subCategory,this.isfavorite=false});

void toggleTheFavoriteStatus(){
  isfavorite=!isfavorite;
  notifyListeners();
 }

}