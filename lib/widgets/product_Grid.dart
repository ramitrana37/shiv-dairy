import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
//import 'package:shivdairystoreapp/providers/Product.dart';
import 'package:shivdairystoreapp/providers/products.dart';
import 'package:shivdairystoreapp/widgets/product_item.dart';



class ProductGrid extends StatelessWidget {
 final bool showFavs;

  const ProductGrid({ this.showFavs} );

  @override
  Widget build(BuildContext context) {
  final  productsData =  Provider.of<Products>(context);
  final products=showFavs?productsData.favs:productsData.items;
    return GridView.builder(
        itemCount: products.length,
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: 2,
          childAspectRatio: 3/2,
          mainAxisSpacing: 3,
          crossAxisSpacing: 3,
        ),
        itemBuilder:(context,i)=>ChangeNotifierProvider.value(
          value: products[i],
//          create: (c,)=>products[i],
          child: ProductItem(),
        )
    );
  }
}





























































//import 'package:provider/provider.dart';
//import 'package:shivdairystoreapp/providers/Products.dart';
//class ProductsGrid extends StatelessWidget {
//
//
//  const ProductsGrid(bool showFavouriteOnly, );
//  @override
//  Widget build(BuildContext context) {
//    final productsData=Provider.of<Products>(context);
//    final productItemsData=productsData.allItems;
//    return GridView.builder(
//      scrollDirection: Axis.vertical,
//      itemCount: productItemsData.length,
//        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
//          crossAxisCount:2 ,
//          crossAxisSpacing:1 ,
//          mainAxisSpacing: 1,
//          childAspectRatio: 3/2,
//        ),
//        itemBuilder: (context,index){return ProductGridItem(
//          productItemsData[index].id,
//          productItemsData[index].title,
//          productItemsData[index].imageUrl,
//        );},
//
//    );
//  }
//}
