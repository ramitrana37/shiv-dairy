import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shivdairystoreapp/providers/Product.dart';
import 'package:shivdairystoreapp/providers/cart.dart';
import 'package:shivdairystoreapp/screens/productsDetailsScreen.dart';



class ProductItem extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final product = Provider.of<Product>(context, listen: false);
    return Padding(padding: const EdgeInsets.all(8.0),
      child: GridTile(

        child: GestureDetector(
          child: Image.network(product.imageUrl, fit: BoxFit.fill), onTap: () {
          Navigator.pushNamed(
              context, ProductsDetailsScreen.screenId, arguments: product.id);
        },),
        footer: GridTileBar(
          backgroundColor: Colors.black54,
          title: Text(product.title),
          leading: Consumer<Product>(
            builder: (context, product, _) =>
                IconButton(
                    icon: product.isfavorite ? Icon(
                      Icons.favorite, color: Colors.red,) : Icon(
                        Icons.favorite_border, color: Colors.red),
                    onPressed: () {
                      product.toggleTheFavoriteStatus();
                    }
                ),
          ),
          trailing: Consumer<Cart>(
            builder: (_, cart, _c) => IconButton(
                icon: Icon(Icons.add_shopping_cart, color: Colors.red),
                onPressed:(){
                  cart.addItemsToCart(product.id,product.title,product.price);
                } ),

          ),
        ),
      ),
    );
  }


}